import { NgModule } from '@angular/core';

import { OdataFrontendSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [OdataFrontendSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [OdataFrontendSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class OdataFrontendSharedCommonModule {}
