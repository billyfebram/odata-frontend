import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { OdataFrontendSharedLibsModule, OdataFrontendSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [OdataFrontendSharedLibsModule, OdataFrontendSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [OdataFrontendSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OdataFrontendSharedModule {
  static forRoot() {
    return {
      ngModule: OdataFrontendSharedModule
    };
  }
}
