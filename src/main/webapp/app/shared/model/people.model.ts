export interface IPeople {
  id?: string;
  description?: string;
  // firstname?: string;
  // lastname?: string;
}

export class People implements IPeople {
  constructor(public id?: string, public description?: string) {}
}
