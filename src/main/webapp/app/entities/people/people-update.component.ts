import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IPeople, People } from 'app/shared/model/people.model';
import { PeopleService } from './people.service';

@Component({
  selector: 'jhi-people-update',
  templateUrl: './people-update.component.html'
})
export class PeopleUpdateComponent implements OnInit {
  people: IPeople;
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    username: [],
    firstname: [],
    lastname: []
  });

  constructor(protected peopleService: PeopleService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ people }) => {
      this.updateForm(people);
      this.people = people;
    });
  }

  updateForm(people: IPeople) {
    this.editForm.patchValue({
      id: people.id,
      username: people.username,
      firstname: people.firstname,
      lastname: people.lastname
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const people = this.createFromForm();
    if (people.id !== undefined) {
      this.subscribeToSaveResponse(this.peopleService.update(people));
    } else {
      this.subscribeToSaveResponse(this.peopleService.create(people));
    }
  }

  private createFromForm(): IPeople {
    const entity = {
      ...new People(),
      id: this.editForm.get(['id']).value,
      username: this.editForm.get(['username']).value,
      firstname: this.editForm.get(['firstname']).value,
      lastname: this.editForm.get(['lastname']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPeople>>) {
    result.subscribe((res: HttpResponse<IPeople>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
